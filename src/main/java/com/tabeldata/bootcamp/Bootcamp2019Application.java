package com.tabeldata.bootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Bootcamp2019Application {

	public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(Bootcamp2019Application.class,
                args);
    }

}
